﻿using System.Collections.Generic;
using System.Linq;
using Kata_Bank_Account.Interface;
using Kata_Bank_Account.Models;
using Kata_Bank_Account.Service;
using Xunit;

namespace Kata_Bank_Account.Tests
{
    public class OperationsServiceTests
    {
        private readonly IOperationService _operationService;

        public OperationsServiceTests()
        {
            _operationService = new OperationService();
        }

        [Theory]
        [InlineData(0, 1, 1)]
        [InlineData(100, -2, 98)]
        [InlineData(-100, 101, 1)]
        [InlineData(100, -200, -100)]
        public void Test_saveMoney(double balance,double amount, double expected)
        {
            Account account = new Account("lastname","firstname", balance);
            _operationService.saveMoney(account, amount);
            IEnumerable<Operation> operations = _operationService.getAllOperations();
            Assert.Equal(expected, operations.FirstOrDefault().AccountOperation.Balance);
        }

        [Theory]
        [InlineData(0, 1, -1)]
        [InlineData(100, -2, 102)]
        [InlineData(-100, 101, -201)]
        [InlineData(100, -200, 300)]
        public void Test_retrieveMoney(double balance, double amount, double expected)
        {
            Account account = new Account("lastname", "firstname", balance);
            _operationService.retrieveMoney(account, amount);
            IEnumerable<Operation> operations = _operationService.getAllOperations();
            Assert.Equal(expected, operations.FirstOrDefault().AccountOperation.Balance);
        }

    }
}
