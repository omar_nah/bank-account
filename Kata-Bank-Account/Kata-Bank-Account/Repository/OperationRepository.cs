﻿using System;
using Kata_Bank_Account.Interface;
using Kata_Bank_Account.Models;

namespace Kata_Bank_Account.Repository
{
    public class OperationRepository : IOperationRepository
    {
        private readonly IOperationService _operationService;
        private Account _account;

        public OperationRepository(IOperationService operationService,Account account)
        {
            this._account = account;
            this._operationService = operationService;
        }

        public void saveMoney(double amount)
        {
            _operationService.saveMoney(_account, amount);
        }

        public void retrieveMoney(double amount)
        {
            _operationService.retrieveMoney(_account, amount);
        }

        public void printOperations()
        {
            Console.WriteLine("Account Id : " + _account.IdAccount);
            Console.WriteLine("Printed at : " + DateTime.Now);
            Console.WriteLine("-----------------------------------");
            foreach (var operation in _operationService.getAllOperations())
            {
                Console.WriteLine("Date --------------- Amount");
                Console.WriteLine(operation.OperationDate+" ------- " + operation.Amount);
            }
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Current balance is : " + _account.Balance);
        }
    }
}
