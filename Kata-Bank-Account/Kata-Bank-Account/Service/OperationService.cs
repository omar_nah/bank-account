﻿using System.Collections.Generic;
using Kata_Bank_Account.Interface;
using Kata_Bank_Account.Models;

namespace Kata_Bank_Account.Service
{
    public class OperationService : IOperationService
    {
        private readonly List<Operation> _operations = new List<Operation>();

        public OperationService()
        {
        }

        public void saveMoney(Account account, double amount)
        {
            _operations.Add(new Operation(account, amount));
        }

        public void retrieveMoney(Account account, double amount)
        {
            _operations.Add(new Operation(account, -amount));
        }

        public IEnumerable<Operation> getAllOperations()
        {
            return _operations;
        }
    }
}
