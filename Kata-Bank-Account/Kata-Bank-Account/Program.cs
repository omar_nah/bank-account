﻿using System;
using Kata_Bank_Account.Interface;
using Kata_Bank_Account.Repository;
using Kata_Bank_Account.Service;

namespace Kata_Bank_Account
{
    class Program
    {
        static void Main(string[] args)
        {
            IOperationRepository operationRepository = new OperationRepository
                (new OperationService(),new Models.Account("Om","ar", 1000.13 ));
            operationRepository.saveMoney(10);
            operationRepository.saveMoney(- 2.3);
            operationRepository.retrieveMoney(3);
            operationRepository.printOperations();
            Console.ReadKey();
        }
    }
}
