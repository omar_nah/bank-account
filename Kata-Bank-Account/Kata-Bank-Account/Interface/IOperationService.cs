﻿using System.Collections.Generic;
using Kata_Bank_Account.Models;

namespace Kata_Bank_Account.Interface
{
    public interface IOperationService
    {
        void saveMoney(Account account, double amount);
        void retrieveMoney(Account account, double amount);
        IEnumerable<Operation> getAllOperations();
    }
}
