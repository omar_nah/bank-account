﻿namespace Kata_Bank_Account.Interface
{
    public interface IOperationRepository
    {
        void saveMoney( double amount);
        void retrieveMoney(double amount);
        void printOperations();
    }
}
