﻿using System;
namespace Kata_Bank_Account.Models
{
    public class Account
    {
        public Guid IdAccount { get; }
        public string LastName { get; }
        public string FirstName { get; }
        public double Balance { get; set; }
        public DateTime CreationDate { get; }

        public Account(string LastName, string FirstName, double Balance)
        {
            this.IdAccount = Guid.NewGuid();
            this.LastName = LastName;
            this.FirstName = FirstName;
            this.Balance = Balance;
            this.CreationDate = DateTime.Now;
        }
    }
}
