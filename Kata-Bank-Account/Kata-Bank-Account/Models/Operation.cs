﻿using System;
namespace Kata_Bank_Account.Models
{
    public class Operation
    {
        public Guid IdOperation { get; }
        public Account AccountOperation { get; }
        public DateTime OperationDate { get; }
        public double Amount { get; }

        public Operation(Account AccountOperation, double amount)
        {
            this.IdOperation = Guid.NewGuid();
            AccountOperation.Balance = AccountOperation.Balance + amount;
            this.AccountOperation = AccountOperation;
            this.Amount = amount;
            this.OperationDate = DateTime.Now;
        }
    }
}
